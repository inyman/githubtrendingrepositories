# Github Trending Repositories

###About 
* App shows the list of **kotlin** repositories with most stars on the GitHub
* Tapping on the Github Repository opens up a new Activity with the small info about the repo

###Used libs and frameworks
* Test driven development - using automation tests (daggermock, espresso, junit, mockito)
* **Kotlin**, Dagger, Retrofit

###Project structure
* DI - dagger dependency injection
* Model - data classes used when deserialize json from the GitHub and also for presentation logic
* Service - RepositoryService is a facade service around currently only one GithubApi service. 
Used to define and manipulate GitHubApi. 
Could be used to hide different implementations (e.g LocalFileSystem, LocalDb, Multiple remote apis ) 
* UI - presentation logic - used MVP pattern with contract  defining View and Presenter

