package com.ivannajman.trendingrepositories.di.module

import com.ivannajman.trendingrepositories.service.RepositoryService
import com.ivannajman.trendingrepositories.ui.repositorylist.RepositoryListContract
import com.ivannajman.trendingrepositories.ui.repositorylist.RepositoryListPresenter
import dagger.Module
import dagger.Provides

@Module
class RepositoryListModule {
    @Provides
    fun providePresenter(repositoryService: RepositoryService): RepositoryListContract.Presenter {
        return RepositoryListPresenter(repositoryService)
    }
}