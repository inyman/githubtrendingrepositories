package com.ivannajman.trendingrepositories.di.module

import com.ivannajman.trendingrepositories.service.RepositoryService
import com.ivannajman.trendingrepositories.ui.repositorydetail.RepositoryDetailContract
import com.ivannajman.trendingrepositories.ui.repositorydetail.RepositoryDetailPresenter
import dagger.Module
import dagger.Provides

@Module
class RepositoryDetailModule {

    @Provides
    fun providePresenter(repositoryService: RepositoryService): RepositoryDetailContract.Presenter {
        return RepositoryDetailPresenter(repositoryService)
    }
}