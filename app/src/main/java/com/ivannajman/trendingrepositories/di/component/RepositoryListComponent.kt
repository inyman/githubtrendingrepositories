package com.ivannajman.trendingrepositories.di.component

import com.ivannajman.trendingrepositories.di.module.RepositoryListModule
import com.ivannajman.trendingrepositories.ui.repositorylist.RepositoryListActivity
import dagger.Subcomponent

@Subcomponent(modules = [RepositoryListModule::class])
interface RepositoryListComponent {

    fun inject(activity: RepositoryListActivity)

    @Subcomponent.Builder
    interface Builder {
        fun listModule(module: RepositoryListModule): Builder

        fun build(): RepositoryListComponent
    }

}