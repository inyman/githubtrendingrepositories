package com.ivannajman.trendingrepositories.di.module

import android.app.Application
import com.ivannajman.trendingrepositories.GithubRepositoryApplication
import com.ivannajman.trendingrepositories.service.GithubApi
import com.ivannajman.trendingrepositories.service.RepositoryService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val app: GithubRepositoryApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return app
    }

    @Provides
    @Singleton
    fun providesRepositoryService(githubApi: GithubApi): RepositoryService {
        return RepositoryService(githubApi)
    }

    @Provides
    @Singleton
    fun provideGithubApi(): GithubApi {
        return GithubApi.create()
    }
}