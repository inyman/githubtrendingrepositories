package com.ivannajman.trendingrepositories.di.component

import com.ivannajman.trendingrepositories.di.module.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    fun detailComponentBuilder(): RepositoryDetailComponent.Builder
    fun listComponentBuilder(): RepositoryListComponent.Builder

}