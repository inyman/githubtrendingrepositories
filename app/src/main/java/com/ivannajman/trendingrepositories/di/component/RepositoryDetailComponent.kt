package com.ivannajman.trendingrepositories.di.component

import com.ivannajman.trendingrepositories.di.module.RepositoryDetailModule
import com.ivannajman.trendingrepositories.ui.repositorydetail.RepositoryDetailFragment
import dagger.Subcomponent

@Subcomponent(modules = [RepositoryDetailModule::class])
interface RepositoryDetailComponent {

    fun inject(detailFragment: RepositoryDetailFragment)

    @Subcomponent.Builder
    interface Builder {
        fun detailModule(module: RepositoryDetailModule): Builder

        fun build(): RepositoryDetailComponent
    }


}