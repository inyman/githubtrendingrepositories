package com.ivannajman.trendingrepositories.ui.repositorydetail

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ivannajman.trendingrepositories.GithubRepositoryApplication
import com.ivannajman.trendingrepositories.R
import com.ivannajman.trendingrepositories.di.module.RepositoryDetailModule
import com.ivannajman.trendingrepositories.model.RepositoryData
import kotlinx.android.synthetic.main.activity_githubrepository_detail.*
import kotlinx.android.synthetic.main.githubrepository_detail.view.*
import javax.inject.Inject

class RepositoryDetailFragment : Fragment(), RepositoryDetailContract.View {

    @Inject
    lateinit var presenter: RepositoryDetailContract.Presenter

    private lateinit var rootView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()

        arguments?.let {
            if (it.containsKey(ARG_ITEM_ID)) {
                presenter.setRepositoryId(it.getString(ARG_ITEM_ID))
            }
        }
    }

    private fun injectDependency() {
        val app = activity?.application as GithubRepositoryApplication

        app.component.detailComponentBuilder()
                .detailModule(RepositoryDetailModule())
                .build()
                .inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.githubrepository_detail, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.attach(this)
        presenter.subscribe()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unsubscribe()
    }

    override fun showData(repositoryData: RepositoryData?) {
        repositoryData?.let {
            rootView.html_url.text = it.html_url
            rootView.description.text = it.description
            activity?.toolbar_layout?.title = it.full_name
        }
    }

    companion object {
        const val FRAGMENT_TAG = "RepositoryDetailFragment"
        const val ARG_ITEM_ID = "item_id"
    }
}
