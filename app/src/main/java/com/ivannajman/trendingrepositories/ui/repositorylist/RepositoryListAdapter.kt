package com.ivannajman.trendingrepositories.ui.repositorylist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ivannajman.trendingrepositories.R
import com.ivannajman.trendingrepositories.model.RepositoryData
import kotlinx.android.synthetic.main.githubrepository_list_content.view.*

class RepositoryListAdapter(listActivity: RepositoryListActivity,
                            private val values: List<RepositoryData>) :
        RecyclerView.Adapter<RepositoryListAdapter.ViewHolder>() {

    private val listener: RepositoryListAdapter.OnItemClickListener

    init {
        this.listener = listActivity
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.githubrepository_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.nameText.text = item.full_name
        holder.descriptionText.text = item.description

        with(holder.itemView) {
            tag = item
            
            setOnClickListener {
                listener.onRepositoryDetailClick(item.id)
            }
        }
    }

    override fun getItemCount() = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameText: TextView = view.githubName
        val descriptionText: TextView = view.description
    }

    interface OnItemClickListener {
        fun onRepositoryDetailClick(repositoryId: String)
    }
}