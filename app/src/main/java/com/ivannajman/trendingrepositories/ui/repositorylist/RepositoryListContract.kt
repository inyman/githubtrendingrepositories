package com.ivannajman.trendingrepositories.ui.repositorylist

import com.ivannajman.trendingrepositories.model.RepositoryData
import com.ivannajman.trendingrepositories.ui.base.BaseContract

class RepositoryListContract {
    interface View : BaseContract.View {
        fun showProgress(show: Boolean)
        fun showError(throwable: Throwable)
        fun loadDataSuccess(list: List<RepositoryData>)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun loadData()
    }
}