package com.ivannajman.trendingrepositories.ui.repositorydetail

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.ivannajman.trendingrepositories.R
import com.ivannajman.trendingrepositories.ui.repositorylist.RepositoryListActivity
import kotlinx.android.synthetic.main.activity_githubrepository_detail.*


class RepositoryDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_githubrepository_detail)
        setSupportActionBar(detail_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        showDetailFragment()
    }

    private fun showDetailFragment() {
        if (supportFragmentManager.findFragmentByTag(RepositoryDetailFragment.FRAGMENT_TAG) == null) {

            val fragment = RepositoryDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(RepositoryDetailFragment.ARG_ITEM_ID,
                            intent.getStringExtra(RepositoryDetailFragment.ARG_ITEM_ID))
                }
            }

            supportFragmentManager.beginTransaction()
                    .add(R.id.githubrepository_detail_container, fragment, RepositoryDetailFragment.FRAGMENT_TAG)
                    .commit()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    NavUtils.navigateUpTo(this, Intent(this, RepositoryListActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

}
