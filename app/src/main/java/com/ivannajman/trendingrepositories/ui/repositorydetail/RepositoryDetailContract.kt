package com.ivannajman.trendingrepositories.ui.repositorydetail

import com.ivannajman.trendingrepositories.model.RepositoryData
import com.ivannajman.trendingrepositories.ui.base.BaseContract

class RepositoryDetailContract {

    interface View : BaseContract.View {
        fun showData(repositoryData: RepositoryData?)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun setRepositoryId(repositoryId: String)
    }
}