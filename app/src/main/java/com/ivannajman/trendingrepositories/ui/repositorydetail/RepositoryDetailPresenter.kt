package com.ivannajman.trendingrepositories.ui.repositorydetail

import com.ivannajman.trendingrepositories.service.RepositoryService

class RepositoryDetailPresenter(var repositoryService: RepositoryService) : RepositoryDetailContract.Presenter {

    private lateinit var repositoryId: String

    override fun setRepositoryId(repositoryId: String) {
        this.repositoryId = repositoryId
    }

    override fun subscribe() {
    }

    override fun unsubscribe() {
    }

    override fun attach(view: RepositoryDetailContract.View) {
        val repositoryData = repositoryService.getRepositoryById(repositoryId)
        view.showData(repositoryData)
    }
}