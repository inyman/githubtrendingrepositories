package com.ivannajman.trendingrepositories.ui.repositorylist

import com.ivannajman.trendingrepositories.model.RepositoryData
import com.ivannajman.trendingrepositories.service.RepositoryService
import io.reactivex.disposables.CompositeDisposable

class RepositoryListPresenter(private var repositoryService: RepositoryService) : RepositoryListContract.Presenter {

    private val subscriptions = CompositeDisposable()
    private lateinit var view: RepositoryListContract.View

    override fun subscribe() {
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: RepositoryListContract.View) {
        this.view = view
    }

    override fun loadData() {
        val subscribe = repositoryService.getTrendingKotlinRepositories()
                .doOnSubscribe { view.showProgress(true) }
                .subscribe(this::onLoadDataComplete, this::onLoadDataError)


        subscriptions.add(subscribe)
    }

    private fun onLoadDataComplete(repositoryData: List<RepositoryData>) {
        view.showProgress(false)
        view.loadDataSuccess(repositoryData)
    }

    private fun onLoadDataError(error: Throwable) {
        view.showProgress(false)
        view.showError(error)
    }
}