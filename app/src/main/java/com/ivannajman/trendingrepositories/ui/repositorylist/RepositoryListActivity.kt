package com.ivannajman.trendingrepositories.ui.repositorylist

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ivannajman.trendingrepositories.GithubRepositoryApplication
import com.ivannajman.trendingrepositories.R
import com.ivannajman.trendingrepositories.di.module.RepositoryListModule
import com.ivannajman.trendingrepositories.model.RepositoryData
import com.ivannajman.trendingrepositories.ui.repositorydetail.RepositoryDetailActivity
import com.ivannajman.trendingrepositories.ui.repositorydetail.RepositoryDetailFragment
import kotlinx.android.synthetic.main.activity_githubrepository_list.*
import kotlinx.android.synthetic.main.githubrepository_list.*
import javax.inject.Inject


class RepositoryListActivity : AppCompatActivity(), RepositoryListContract.View, RepositoryListAdapter.OnItemClickListener {

    @Inject
    lateinit var presenter: RepositoryListContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_githubrepository_list)
        setSupportActionBar(toolbar)
        toolbar.title = title

        injectDependency()
        presenter.attach(this)
        presenter.loadData()
    }

    private fun injectDependency() {
        val app = application as GithubRepositoryApplication

        app.component.listComponentBuilder()
                .listModule(RepositoryListModule())
                .build()
                .inject(this)

    }

    override fun onRepositoryDetailClick(repositoryId: String) {
        val intent = Intent(applicationContext, RepositoryDetailActivity::class.java).apply {
            putExtra(RepositoryDetailFragment.ARG_ITEM_ID, repositoryId)
        }

        startActivity(intent)
    }

    override fun showProgress(show: Boolean) {
        progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun showError(throwable: Throwable) {
        showError(throwable.localizedMessage)
    }

    override fun loadDataSuccess(list: List<RepositoryData>) {
        trendingRepositoryList.adapter = RepositoryListAdapter(this, list)

        if (list.isEmpty()) {
            showError(getString(R.string.no_results))
        } else {
            errorText.visibility = View.GONE
        }
    }

    private fun showError(error: String) {
        errorText.visibility = View.VISIBLE
        errorText.text = error
    }
}
