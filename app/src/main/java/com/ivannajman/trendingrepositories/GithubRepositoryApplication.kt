package com.ivannajman.trendingrepositories

import android.app.Application
import com.ivannajman.trendingrepositories.di.component.ApplicationComponent
import com.ivannajman.trendingrepositories.di.component.DaggerApplicationComponent
import com.ivannajman.trendingrepositories.di.module.ApplicationModule

class GithubRepositoryApplication : Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        component = createComponent()
    }

    private fun createComponent(): ApplicationComponent {
        return DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
    }
}