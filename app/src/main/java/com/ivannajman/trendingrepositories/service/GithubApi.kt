package com.ivannajman.trendingrepositories.service

import com.ivannajman.trendingrepositories.model.RepositoryResponse
import io.reactivex.Observable
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface GithubApi {

    @GET("search/repositories")
    fun searchRepositories(@Query("q") query: String,
                           @Query("sort") sort: String,
                           @Query("order") order: String): Observable<RepositoryResponse>


    companion object Factory {

        fun create(): GithubApi {
            val retrofit = retrofit2.Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(MoshiConverterFactory.create())
                    .baseUrl(GithubConstants.Url)
                    .build()

            return retrofit.create(GithubApi::class.java)
        }
    }
}