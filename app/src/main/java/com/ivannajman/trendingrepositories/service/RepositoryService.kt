package com.ivannajman.trendingrepositories.service

import com.ivannajman.trendingrepositories.model.RepositoryData
import com.ivannajman.trendingrepositories.model.RepositoryResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

class RepositoryService(private val githubApi: GithubApi) {

    fun getTrendingKotlinRepositories(): Observable<List<RepositoryData>> {

//        val date = getTrendingStartDate()
//        "language:kotlin created:$date"
        
        return githubApi.searchRepositories("language:kotlin", "stars", "desc")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(RepositoryResponse::items)
                .map(this@RepositoryService::cacheRepositoryData)
    }

    fun getRepositoryById(repositoryId: String): RepositoryData? {
        return LOCAL_CACHE[repositoryId]
    }

    private fun getTrendingStartDate(): String? {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, -7)

        return simpleDateFormat.format(calendar.time)
    }

    private fun cacheRepositoryData(repositoryData: List<RepositoryData>): List<RepositoryData> {
        repositoryData.map {
            LOCAL_CACHE.put(it.id, it)
        }

        return repositoryData
    }

    companion object {
        val LOCAL_CACHE: MutableMap<String, RepositoryData> = HashMap()
    }
}