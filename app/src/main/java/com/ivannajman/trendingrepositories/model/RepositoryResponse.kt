package com.ivannajman.trendingrepositories.model

data class RepositoryResponse(val items: List<RepositoryData>)