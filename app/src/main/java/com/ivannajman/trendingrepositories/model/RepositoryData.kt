package com.ivannajman.trendingrepositories.model

data class RepositoryData(val id: String,
                          val full_name: String,
                          val html_url: String,
                          val description: String,
                          val language: String)