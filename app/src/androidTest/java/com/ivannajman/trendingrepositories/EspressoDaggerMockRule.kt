package com.ivannajman.trendingrepositories

import android.support.test.InstrumentationRegistry
import com.ivannajman.trendingrepositories.di.component.ApplicationComponent
import com.ivannajman.trendingrepositories.di.module.ApplicationModule
import it.cosenonjaviste.daggermock.DaggerMock

fun espressoDaggerMockRule() = DaggerMock.rule<ApplicationComponent>(ApplicationModule(app)) {
    set { component -> app.component = component }
}

val app: GithubRepositoryApplication get() = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as GithubRepositoryApplication