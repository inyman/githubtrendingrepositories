package com.ivannajman.trendingrepositories.ui.repositorydetail

import android.content.Intent
import android.support.design.widget.CollapsingToolbarLayout
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.ivannajman.trendingrepositories.R
import com.ivannajman.trendingrepositories.espressoDaggerMockRule
import com.ivannajman.trendingrepositories.model.RepositoryData
import com.ivannajman.trendingrepositories.service.RepositoryService
import com.nhaarman.mockito_kotlin.mock
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn


@RunWith(AndroidJUnit4::class)
@LargeTest
class RepositoryDetailActivityTest {
    @get:Rule
    val rule = espressoDaggerMockRule()

    @get:Rule
    val activityRule = ActivityTestRule(RepositoryDetailActivity::class.java, false, false)

    @Mock
    val repositoryService: RepositoryService = mock()

    @Test
    fun showRepositoryDetail() {
        val repositoryData = RepositoryData("repositoryId",
                "Repository full name",
                "html",
                "desc",
                "kotlin")

        doReturn(repositoryData).`when`<RepositoryService>(repositoryService).getRepositoryById("repositoryId")

        val intent = Intent()
        intent.putExtra(RepositoryDetailFragment.ARG_ITEM_ID, "repositoryId")

        activityRule.launchActivity(intent)

        onView(withId(R.id.html_url)).check(matches(withText("html")))
        onView(withId(R.id.description)).check(matches(withText("desc")))
        onView(withId(R.id.toolbar_layout)).check(matches(withToolbarTitle(("Repository full name"))))
    }

    private fun withToolbarTitle(expected: String): Matcher<Any> {
        return object : BoundedMatcher<Any, CollapsingToolbarLayout>(CollapsingToolbarLayout::class.java!!) {
            public override fun matchesSafely(toolbar: CollapsingToolbarLayout): Boolean {
                return toolbar.title == expected
            }

            override fun describeTo(description: Description) {
                description.appendText("Not found : $expected")
            }
        }
    }
}