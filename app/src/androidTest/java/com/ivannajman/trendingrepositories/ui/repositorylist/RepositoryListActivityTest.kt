package com.ivannajman.trendingrepositories.ui.repositorylist

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.NoMatchingViewException
import android.support.test.espresso.ViewAssertion
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import android.view.View
import com.ivannajman.trendingrepositories.R
import com.ivannajman.trendingrepositories.espressoDaggerMockRule
import com.ivannajman.trendingrepositories.model.RepositoryData
import com.ivannajman.trendingrepositories.model.RepositoryResponse
import com.ivannajman.trendingrepositories.service.RepositoryService
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Observable
import org.hamcrest.CoreMatchers.`is`
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.doReturn


@RunWith(AndroidJUnit4::class)
@LargeTest
class RepositoryListActivityTest {

    @get:Rule
    val rule = espressoDaggerMockRule()

    @get:Rule
    val activityRule = ActivityTestRule(RepositoryListActivity::class.java, false, false)

    @Mock
    val repositoryService: RepositoryService = mock()

    @Test
    fun showErrorWhenServerIsDown() {
        val repositoryServiceResponse = Observable.error<RepositoryResponse>(Throwable("Server is down"))
        doReturn(repositoryServiceResponse).`when`<RepositoryService>(repositoryService).getTrendingKotlinRepositories()

        activityRule.launchActivity(null)

        onView(withId(R.id.errorText)).check(matches(withText("Server is down")))
    }


    @Test
    fun showRepositoriesFromServer() {
        val repositoryData = listOf(RepositoryData("1", "name", "html", "desc", "kotlin"))

        val repositoryServiceResponse = Observable.just(repositoryData)
        doReturn(repositoryServiceResponse).`when`<RepositoryService>(repositoryService).getTrendingKotlinRepositories()

        activityRule.launchActivity(null)

        onView(withId(R.id.trendingRepositoryList)).check(RecyclerViewItemCountAssertion(1))
        onView(withId(R.id.errorText)).check(matches(withText("")))
    }

    @Test
    fun showNoResults() {
        val repositoryData = emptyList<RepositoryData>()

        val repositoryServiceResponse = Observable.just(repositoryData)
        doReturn(repositoryServiceResponse).`when`<RepositoryService>(repositoryService).getTrendingKotlinRepositories()

        activityRule.launchActivity(null)

        onView(withId(R.id.trendingRepositoryList)).check(RecyclerViewItemCountAssertion(0))
        onView(withId(R.id.errorText)).check(matches(withText(R.string.no_results)))
    }

    inner class RecyclerViewItemCountAssertion(private val expectedCount: Int) : ViewAssertion {

        override fun check(view: View, noViewFoundException: NoMatchingViewException?) {
            if (noViewFoundException != null) {
                throw noViewFoundException
            }

            val recyclerView = view as RecyclerView
            val adapter = recyclerView.adapter
            assertThat(adapter.itemCount, `is`(expectedCount))
        }
    }
}